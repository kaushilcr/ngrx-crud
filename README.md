  # Crud Angular Ngrx

This is a basic crud example of how you can can extend a base component and service that have all the similar functions and states between yours components. This repository also uses ngrx store/effects.

## Installing

Just run `npm install` to install all the dependencies.

## Development server

Run `json-server --watch database.json` to start the fake api at `http://localhost:3000/`;
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
