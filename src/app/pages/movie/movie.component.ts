import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Movie } from 'src/app/models/movie.model';
import { MovieService } from 'src/app/services/movie.service';
import { BaseResourceGridComponent } from 'src/app/utils/base-resource-grid.component';
import { movieActions } from '../../store/modules/movie/movie.action';
import { getAllMovies } from '../../store/modules/movie/movie.selectors';

const createFormGroup = (dataItem?: Movie) => {
  if (dataItem) {
    return new FormGroup({
      id: new FormControl(dataItem.id),
      name: new FormControl(dataItem.name, Validators.required),
      director: new FormControl(dataItem.director, Validators.required)
    });
  }
  return new FormGroup({
    id: new FormControl(),
    name: new FormControl('', Validators.required),
    director: new FormControl('', Validators.required)
  });
};

@Component({
  selector: 'movie-grid',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieGrid extends BaseResourceGridComponent<Movie>
  implements OnInit {
  count$: Observable<number>;
  movies$: Observable<Movie[]> = this.store.pipe(select(getAllMovies));
  public clearFormSubscription = new Subscription();
  displayedColumns: string[] = ['name', 'director'];
  columnsToDisplay: string[] = [...this.displayedColumns, 'action'];

  constructor(
    protected injector: Injector,
    protected movieService: MovieService,
    protected store: Store
  ) {
    super(
      injector,
      new Movie(),
      movieService,
      createFormGroup,
      movieActions,
      store
    );
  }
}
