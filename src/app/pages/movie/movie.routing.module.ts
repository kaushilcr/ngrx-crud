import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieGrid } from './movie.component';

const routes: Routes = [{ path: '', component: MovieGrid }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule {}
