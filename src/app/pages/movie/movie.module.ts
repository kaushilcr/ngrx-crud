import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MovieEffects } from 'src/app/store/modules/movie/movie.effects';
import {
  movieFeatureKey,
  movieReducer
} from '../../store/modules/movie/movie.reducer';
import { MovieGrid } from './movie.component';
import { MovieRoutingModule } from './movie.routing.module';
@NgModule({
  declarations: [MovieGrid],
  imports: [
    CommonModule,
    MovieRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature(movieFeatureKey, movieReducer),
    EffectsModule.forFeature([MovieEffects]),
    MatInputModule,
    MatButtonModule,
    MatTableModule
  ],
  entryComponents: [],
  providers: []
})
export class MovieModule {}
