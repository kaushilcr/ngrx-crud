import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MovieState } from './movie.reducer';

export const getAllMovieState = createFeatureSelector<MovieState>('movies');
export const getMovieEditingState = createFeatureSelector<MovieState>('movies');

export const getAllMovies = createSelector(
  getAllMovieState,
  (state: MovieState) => state.movies
);

export const getMovieEditing = createSelector(
  getMovieEditingState,
  (state: MovieState) => state.editingMovie
);
