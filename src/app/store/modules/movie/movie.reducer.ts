import { Action, createReducer, on } from '@ngrx/store';
import { Movie } from '../../../models/movie.model';
import { loadedA } from './movie.action';

export interface MovieState {
  movies: Movie[];
  editingMovie: Movie;
}

export const initialState: MovieState = {
  movies: [],
  editingMovie: {
    id: null,
    director: '',
    name: ''
  }
};

export const movieFeatureKey = 'movies';

const _movieReducer = createReducer(
  initialState,
  on(loadedA, (state, { movies }) => ({ ...state, movies: movies }))
);

export function movieReducer(state: MovieState | undefined, action: Action) {
  return _movieReducer(state, action);
}
