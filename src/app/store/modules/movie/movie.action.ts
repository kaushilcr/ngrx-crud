import { createAction, props } from '@ngrx/store';
import { Movie } from '../../../models/movie.model';

export const loadA = createAction('[Movies List] Load Movie List via Service');

export const loadedA = createAction(
  '[Movies Effect] Movie List Loaded Successfully',
  props<{ movies: Movie[] }>()
);

export const createA = createAction(
  '[Create Movie Component] Create Movie',
  props<{ data: Movie }>()
);

export const deleteA = createAction(
  '[Movie List Operations] Delete Movie',
  props<{ id: number }>()
);

export const updateA = createAction(
  '[Movie List Operations] Update Movie',
  props<{ data: Movie }>()
);

export const movieActions = {
  loadA,
  loadedA,
  createA,
  deleteA,
  updateA
};
