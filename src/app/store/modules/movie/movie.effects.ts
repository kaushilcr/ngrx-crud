import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import { map, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { MovieService } from '../../../services/movie.service';
import { movieActions } from './movie.action';

@Injectable()
export class MovieEffects {
  constructor(private actions$: Actions, private movieService: MovieService) {}

  loadMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(movieActions.loadA),
      mergeMap(() =>
        this.movieService.getAll().pipe(
          map((movies: any) => movieActions.loadedA({ movies: movies })),
          catchError(() => EMPTY)
        )
      )
    )
  );

  public saveMovie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(movieActions.createA),
      switchMap((action) =>
        this.movieService.save(action.data).pipe(
          map(() => {
            this.movieService.setClearForm();
            return movieActions.loadA();
          })
        )
      )
    )
  );

  public updateMovie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(movieActions.updateA),
      switchMap((action) =>
        this.movieService.update(action.data).pipe(
          map(
            () => {
              this.movieService.setClearForm();
              return movieActions.loadA();
            },
            catchError((error) => of(movieActions.loadA()))
          )
        )
      )
    )
  );

  public deleteMovie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(movieActions.deleteA),
      switchMap((action) =>
        this.movieService.delete(action.id).pipe(
          map(() => movieActions.loadA()),
          catchError((error) => of(movieActions.loadA()))
        )
      )
    )
  );
}
