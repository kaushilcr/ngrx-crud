export class Movie {
  constructor(
    public id?: number,
    public name?: string,
    public director?: string
  ) {}
  static fromJson(jsonData: any): Movie {
    return Object.assign(new Movie(jsonData.id), jsonData);
  }
}
