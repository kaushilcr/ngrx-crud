import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import './pages/movie/movie.module';

const routes: Routes = [
  { path: '', loadChildren: './pages/movie/movie.module#MovieModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
