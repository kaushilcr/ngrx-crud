import { Injectable, Injector } from '@angular/core';
import { Subject } from 'rxjs';
import { Movie } from '../models/movie.model';
import { BASE_URL } from './api.baseUrl';
import { BaseResourceService } from './base-resource-service';

@Injectable({
  providedIn: 'root'
})
export class MovieService extends BaseResourceService<Movie> {
  constructor(protected injector: Injector) {
    super(`${BASE_URL}/movies`, injector, Movie.fromJson);
  }
}
